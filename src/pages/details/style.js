import Constants from 'expo-constants'
import { StyleSheet } from 'react-native'

export default StyleSheet.create({
  container: {
    flex: 1,
    paddingHorizontal: 24,
    paddingTop: Constants.statusBarHeight + 10
  },
  header: {
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'space-between'
  },
  case: {
    padding: 24,
    borderRadius: 8,
    backgroundColor: "#fff",
    marginBottom: 16,
    marginTop: 30
  },
  caseProp: {
    fontSize: 16,
    color: "#41414d",
    fontWeight: 'bold'
  },
  caseValue: {
    marginTop: 8,
    fontSize: 18,
    marginBottom: 24,
    color: '#737380'
  },
  contactBox: {
    padding: 24,
    borderRadius: 8,
    backgroundColor: "#fff",
    marginBottom: 16
  },
  hero: { alignItems: 'center'},
  heroTitle: {
    alignItems: 'center',
    fontWeight: 'bold',
    fontSize: 20,
    color: '#13131a',
    lineHeight: 30
  },
  heroDescription: {
    fontSize: 20,
    color: '#737380',
    marginTop: 15
  },
  actions: {
    marginTop: 16,
    flexDirection: "row",
    justifyContent: 'space-around'
  },
  action: {
    backgroundColor: '#e02041',
    borderRadius: 8,
    height: 50,
    width: '40%',
    justifyContent: 'center',
    alignItems: 'center',
  },
  actionText: {
    color: '#fff',
    fontSize: 20,
    fontWeight: 'bold'
  }
})