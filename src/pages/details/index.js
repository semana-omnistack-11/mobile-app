import React from 'react'
import style from './style'
import logo from '../../assets/logo.png'
import { useNavigation, useRoute } from '@react-navigation/native'
import { Feather } from '@expo/vector-icons'
import { composeAsync } from 'expo-mail-composer'
import { View, Text, Image, TouchableOpacity, Linking } from 'react-native'
import { NumberFormat } from 'intl'
 
export default function Detail(props) {
  const navigation = useNavigation()
  const route = useRoute()
  const incident = route.params.incident
  const message = `Olá ${incident.name}
  Estou mandando essa mensagem, pois gostaria de tratar com vocês sobre como posso ajudá-los com o caso "${incident.description}" com a quantia de R$ ${incident.value}
  
  Atenciosamente`

  function back_to_the_cases() {
    navigation.goBack()
  }

  function send_email() {
    composeAsync({
      subject: `Herói do caso: ${incident.title}`,
      recipients: [incident.email],
      body: message
    })
  }

  function send_whatsapp() {
    Linking.openURL(`whatsapp://send?phone=${incident.whatsapp}&text=${message}`)
  }

  return(
    <View style={style.container}>
      <View style={style.header}>
        <Image source={logo} />
        
        <TouchableOpacity onPress={back_to_the_cases}>
          <Feather 
            name='arrow-left'
            size={30}
            color='#e02041' />
        </TouchableOpacity>
      </View>

      <View style={style.case}>
        <Text style={style.caseProp}> ONG: </Text>
        <Text style={style.caseValue}> { incident.name } </Text>

        <Text style={style.caseProp}> Caso: </Text>
        <Text style={style.caseValue}> { incident.title } </Text>

        <Text style={style.caseProp}> Valor: </Text>
        <Text style={style.caseValue}> R$ { incident.value } </Text>
      </View>

      <View style={style.contactBox}>
        <View style={style.hero}>
          <Text style={style.heroTitle}> Salve o dia !! </Text>
          <Text style={style.heroTitle}> Seja o herói deste caso </Text>
          
          <Text style={style.heroDescription}> Entre em contato: </Text>
        </View>

        <View style={style.actions}>
          <TouchableOpacity 
            style={style.action} onPress={send_whatsapp}>
            <Text style={style.actionText}> WhatsApp </Text>
          </TouchableOpacity>

          <TouchableOpacity 
            style={style.action} onPress={send_email}>
            <Text style={style.actionText}> Email </Text>
          </TouchableOpacity>
        </View>
      </View>
    </View>
  )
}