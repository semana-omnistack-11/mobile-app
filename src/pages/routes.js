import React from 'react'
import { createStackNavigator } from '@react-navigation/stack'
import { NavigationContainer } from '@react-navigation/native'

import Cases from './cases'
import Detail from './details'

const AppStack = createStackNavigator()

export default function Routes() {
  return(
    <NavigationContainer>
      <AppStack.Navigator screenOptions={{headerShown: false}}>
        <AppStack.Screen name="Cases" component={Cases} />
        <AppStack.Screen name="Details" component={Detail} />
      </AppStack.Navigator>
    </NavigationContainer>
  )
}