import style from './style'
import api from '../../services/api'
import logo from '../../assets/logo.png'
import React, { useEffect, useState } from 'react'
import { useNavigation } from '@react-navigation/native'
import { Feather } from '@expo/vector-icons'
import { 
  View, 
  Text, 
  Image, 
  FlatList, 
  TouchableOpacity 
} from 'react-native'

// 88a0387875
export default function Cases() {
  const navigation = useNavigation()
  const [incidents, set_incidents] = useState([])
  const [page, setPage] = useState(1)
  const [loading, setLoading] = useState(false)

  function goToDetails(incident) {
    navigation.navigate('Details', { incident })
  }

  async function loadCases() {
    if(loading) { return; }

    setLoading(true)
    const resp = await api.get('cases', {
      params: { page }
    })
    
    set_incidents([...incidents, ...resp.data])
    setPage(page + 1)
    setLoading(false)
  }
  useEffect(() => {
    loadCases()
  }, [])

  return(
    <View style={style.container}>
      <View style={style.header}>
        <Image source={logo}/>
        <Text style={style.headerText}> 
          Total de { incidents.length } caso(s) 
        </Text> 
      </View>

      <Text style={style.title}> Bem Vindo(a)! </Text>
      <Text style={style.description}>
        Escolha um desses casos e começe a salvar o dia
      </Text>

      <FlatList
        data={incidents}
        style={style.casesList}
        keyExtractor={incident => String(incident.id)}
        showsVerticalScrollIndicator={false}
        renderItem={({ item: incident }) => (
          <View style={style.case}>
            <Text style={style.caseProp}> ONG: </Text>
            <Text style={style.caseValue}> { incident.name } </Text>

            <Text style={style.caseProp}> Caso: </Text>
            <Text style={style.caseValue}> { incident.title } </Text>

            <Text style={style.caseProp}> Valor: </Text>
            <Text style={style.caseValue}> R$ { incident.value } </Text>

            <TouchableOpacity 
              style={style.datailsButton}
              onPress={() => goToDetails(incident)} >
                <Text style={style.datailsButtonText}>
                  Ver mais detalhes
                </Text>
                <Feather name="arrow-right" size={20} color="#E02041" />
            </TouchableOpacity>
          </View>
        )} />

    </View>
  )
}