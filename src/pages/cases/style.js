import Constants from 'expo-constants'
import { StyleSheet } from 'react-native'

export default StyleSheet.create({
  container: {
    flex: 1,
    paddingHorizontal: 24,
    paddingTop: Constants.statusBarHeight + 10
  },
  header: {
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'space-between'
  },
  headerText: {
    fontSize: 16,
    fontWeight: 'bold',
    color: '#931310'
  },
  title: {
    fontSize: 30,
    marginBottom: 15,
    marginTop: 25,
    color: '#13131a',
    fontWeight: 'bold'
  },
  description: {
    fontSize: 16,
    lineHeight: 24,
    color: '#737380'
  },
  casesList: { marginTop: 32 },
  case: {
    padding: 24,
    borderRadius: 8,
    backgroundColor: "#fff",
    marginBottom: 16
  },
  caseProp: {
    fontSize: 16,
    color: "#41414d",
    fontWeight: 'bold'
  },
  caseValue: {
    marginTop: 8,
    fontSize: 18,
    marginBottom: 24,
    color: '#737380'
  },
  datailsButton: {
    flexDirection: "row",
    justifyContent: 'space-around',
    alignItems: 'center',
  },
  datailsButtonText: {
    color: '#E02041',
    fontSize: 20,
    fontWeight: 'bold'
  }
})